from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class FormField(models.Model):
    title = models.CharField(max_length=100)
    form_choices = [('text', 'text'), ('radio', 'radio'), ('dropdown', 'dropdown')]
    #    form_choices = ['text', 'radio', 'dropdown']
    type = models.CharField(max_length=50, choices=form_choices)
    details = models.CharField(max_length=100)
    required = bool

    def __str__(self):
        return self.title


class FormBody(models.Model):
    title = models.CharField(max_length=100)
    details = models.CharField(max_length=100)
    fields = models.ManyToManyField(FormField)

    def __str__(self):
        return self.title


class FormButton(MPTTModel):
    name = models.CharField(max_length=100)
    forms_body = models.ManyToManyField(FormBody)  # allow_null=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
