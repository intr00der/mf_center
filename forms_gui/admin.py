from django.contrib import admin
from forms_gui.models import FormBody, FormField

admin.site.register(FormBody)
admin.site.register(FormField)