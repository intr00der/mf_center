from django.shortcuts import render
from .models import FormBody, FormField
from django.views.generic import CreateView, ListView


# def home(request):
#     context = {
#         'posts': Post.objects.all()
#     }
#     return render(request, 'blog/home.html', context)



#
# class FormBodyListView(ListView):
#     model =

class FormBodyFormsView(CreateView):
    model = FormBody
    fields = ['title', 'content']
    template_name = 'forms_gui/formbodyforms.html'
    context_object_name = 'form-body'
